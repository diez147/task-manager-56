package ru.tsc.babeshko.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.babeshko.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.babeshko.tm.dto.model.ProjectDTO;
import ru.tsc.babeshko.tm.dto.model.TaskDTO;
import ru.tsc.babeshko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.babeshko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;

@Service
public class ProjectTaskServiceDTO implements IProjectTaskServiceDTO {

    @NotNull
    @Autowired
    private IProjectServiceDTO projectService;

    @NotNull
    @Autowired
    private ITaskServiceDTO taskService;

    @Override
    public void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final ProjectDTO project = projectService.findOneById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        task.setProjectId(projectId);
        taskService.update(task);
    }

    @Override
    public void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final ProjectDTO project = projectService.findOneById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        task.setProjectId(null);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@NotNull String userId, @NotNull String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new ProjectNotFoundException();
        taskService.removeAllByProjectId(userId, projectId);
        projectService.removeById(userId, projectId);
    }

}