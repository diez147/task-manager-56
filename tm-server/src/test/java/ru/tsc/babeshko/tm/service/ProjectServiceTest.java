package ru.tsc.babeshko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.babeshko.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.babeshko.tm.configuration.ContextConfiguration;
import ru.tsc.babeshko.tm.dto.model.ProjectDTO;
import ru.tsc.babeshko.tm.dto.model.UserDTO;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.exception.field.*;
import ru.tsc.babeshko.tm.util.DateUtil;

import java.util.List;
import java.util.UUID;

public class ProjectServiceTest {

    @NotNull
    private IProjectServiceDTO projectService;

    @NotNull
    private IUserServiceDTO userService;

    @NotNull
    private ITaskServiceDTO taskService;

    private String userId;

    private long initSize;

    private String projectId;

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ContextConfiguration.class);
        projectService = context.getBean(IProjectServiceDTO.class);
        userService = context.getBean(IUserServiceDTO.class);
        taskService = context.getBean(ITaskServiceDTO.class);
        @NotNull final UserDTO user = userService.create("user", "user");
        userId = user.getId();
        @NotNull final ProjectDTO project = projectService.create(userId, "test-1");
        initSize = projectService.getCount();
        projectId = project.getId();
    }

    @After
    public void end() {
        projectService.clear(userId);
        userService.removeByLogin("user");
    }

    @Test
    public void create() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.create("", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> projectService.create(userId, ""));
        projectService.create(userId, "test");
        Assert.assertEquals(initSize + 1, projectService.getCount());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.create("", "test", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> projectService.create(userId, "", "test"));
        Assert.assertThrows(EmptyDescriptionException.class, () -> projectService.create(userId, "test", ""));
        projectService.create(userId, "test", "test");
        Assert.assertEquals(initSize + 1, projectService.getCount());
    }

    @Test
    public void createWithDescriptionAndDate() {
        @Nullable final ProjectDTO project = projectService.create(
                userId,
                "test",
                "test",
                DateUtil.toDate("10.10.2021"),
                DateUtil.toDate("11.11.2021")
        );
        Assert.assertEquals(initSize + 1, projectService.getCount());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getDateBegin());
        Assert.assertNotNull(project.getDateEnd());
    }

    @Test
    public void clear() {
        projectService.clear();
        Assert.assertEquals(0, projectService.getCount());
    }

    @Test
    public void findAll() {
        @NotNull final List<ProjectDTO> projectsAll = projectService.findAll();
        Assert.assertEquals(initSize, projectsAll.size());
        @NotNull final List<ProjectDTO> projectsOwnedUser1 = projectService.findAll(userId);
        Assert.assertEquals(1, projectsOwnedUser1.size());
        @NotNull final List<ProjectDTO> projectsOwnedUser3 = projectService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, projectsOwnedUser3.size());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectService.updateById("", projectId, "test", "test"));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectService.updateById(userId, "", "test", "test"));
        Assert.assertThrows(EmptyNameException.class,
                () -> projectService.updateById(userId, projectId, "", "test"));
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        projectService.updateById(userId, projectId, newName, newDescription);
        @NotNull final ProjectDTO project = projectService.findOneById(projectId);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectService.changeStatusById("", projectId, newStatus));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectService.changeStatusById(userId, "", newStatus));
        projectService.changeStatusById(userId, projectId, newStatus);
        @NotNull final ProjectDTO project = projectService.findOneById(projectId);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(newStatus, project.getStatus());
    }

    @Test
    public void findOneById() {
        @NotNull final String projectName = "test find by id";
        @NotNull final ProjectDTO project = projectService.create(userId, projectName);
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(EmptyIdException.class, () -> projectService.findOneById(""));
        Assert.assertNotNull(projectService.findOneById(projectId));
        Assert.assertEquals(projectName, projectService.findOneById(projectId).getName());
        Assert.assertNotNull(projectService.findOneById(userId, projectId));
        Assert.assertEquals(projectName, projectService.findOneById(userId, projectId).getName());
    }

    @Test
    public void existsById() {
        @NotNull final String projectName = "test exist by id";
        @NotNull final ProjectDTO project = projectService.create(userId, projectName);
        @NotNull final String projectId = project.getId();
        Assert.assertTrue(projectService.existsById(projectId));
        Assert.assertFalse(projectService.existsById(UUID.randomUUID().toString()));
    }

    @Test
    public void remove() {
        @NotNull final ProjectDTO project = projectService.create(userId, "test");
        projectService.remove(project);
        Assert.assertEquals(initSize, projectService.getCount());
        projectService.add(project);
        projectService.remove(userId, project);
        Assert.assertEquals(initSize, projectService.getCount());
    }

    @Test
    public void removeById() {
        @NotNull final ProjectDTO project = projectService.create(userId, "test");
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(EmptyIdException.class, () -> projectService.removeById(""));
        projectService.removeById(projectId);
        Assert.assertEquals(initSize, projectService.getCount());
        projectService.add(project);
        Assert.assertThrows(EmptyIdException.class, () -> projectService.removeById(userId, ""));
        projectService.removeById(userId, projectId);
        Assert.assertNull(projectService.findOneById(userId, UUID.randomUUID().toString()));
        Assert.assertEquals(initSize, projectService.getCount());
    }

    @Test
    public void removeAllByUserId() {
        projectService.create(userId, "test");
        projectService.create(userId, "test2");
        Assert.assertEquals(initSize + 2, projectService.getCount());
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.removeAllByUserId(""));
        projectService.removeAllByUserId(userId);
        Assert.assertEquals(0, projectService.getCount());
    }

}