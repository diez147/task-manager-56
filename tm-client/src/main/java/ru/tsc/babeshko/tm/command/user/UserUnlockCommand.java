package ru.tsc.babeshko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.dto.request.UserUnlockRequest;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.util.TerminalUtil;

@Component
public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-unlock";

    @NotNull
    public static final String DESCRIPTION = "Unlock user.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER LOGIN:]");
        @NotNull String login = TerminalUtil.nextLine();
        getUserEndpoint().unlockUser(new UserUnlockRequest(getToken(), login));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}